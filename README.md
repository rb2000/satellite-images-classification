# Satellite images classification example

This is a fork from the DL4J example_ FitFromFeaturized_ class (
https://github.com/eclipse/deeplearning4j-examples/blob/master/dl4j-examples/src/main/java/org/deeplearning4j/examples/advanced/features/transferlearning/editlastlayer/presave/FitFromFeaturized.java), adapted to process the satellite dataset from the University of California-Merced (http://weegee.vision.ucmerced.edu/datasets/landuse.html ), instead of using the flowers dataset in the original example.
The umerced dataset contains 2100 images covering 21 categories,100 image per category. Each image is 256x256 pixels in size, and has a resolution of about 35 cm per pixel.  Those are the categories in that dataset:

1. agricultural
2. airplane
3. baseballdiamond
4. beach
5. buildings
6. chaparral
7. denseresidential
8. forest
9. freeway
10. golfcourse
11. harbor
12. intersection
13. mediumresidential
14. mobilehomepark
15. overpass
16. parkinglot
17. river
18. runway
19. sparseresidential
20. storagetanks
21. tenniscourt

Here some examples of that dataset:

**Airplanes:**

![ariplanes](/uploads/eef03f2b69de3ccc60741419e5375650/ariplanes.png)

**Freeways:**

![freeway](/uploads/3fd6e512891d92a8c7633212be68dc0c/freeway.png)

To get more information on the dataset see 
> Yi Yang and Shawn Newsam, "Bag-Of-Visual-Words and Spatial Extensions for Land-Use Classification," ACM SIGSPATIAL International Conference on Advances in Geographic Information Systems (ACM GIS), 2010.

The DL4J example uses TransferLearning - the lower layers of a VGG16 pretrained model are frozen, and the output layers are changed :
```java
//original code DL4J 
    ComputationGraph vgg16Transfer = new TransferLearning.GraphBuilder(vgg16Clone)
                .fineTuneConfiguration(fineTuneConf)
                .setFeatureExtractor(featureExtractionLayer) //the specified layer and below are "frozen"
                .removeVertexKeepConnections("predictions") //replace the functionality of the final vertex
                .addLayer("predictions",
                        new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                                .nIn(4096).nOut(numClasses)
                                .weightInit(new NormalDistribution(0, 0.2 * (2.0 / (4096 + numClasses)))) //This weight init dist gave better results than Xavier
                                .activation(Activation.SOFTMAX).build(),
                        "fc2")
                .build();
```

The original VGG16 model uses softmax to classify the 1000 classes present in the ImageNet dataset.
The code in this project sets the number of outputs to 21, as we have 21 satellite image categories.
When running for the first time, it checks if the VGG16 pretrained model("vgg16_dl4j_inference.zip") is present in the \<home directory\>/.deeplearning4j/models/" dir. If not, that model will be downloaded (this model is about 500 MB in size).

### USAGE:
first clean/compile/package the code with the command:
```sh
 mvn clean package
```

this will create a .jar file under the target directory. Go into that directory and call the main class with 3
parameters:

1) the parent directory that contains the subdirectories with the images
2) the absolute path where to save the trained model
3) the number of epochs to train the model.

**Example:**
```sh
satellite-images-classification>  mvn clean package 
satellite-images-classification>  cd target
satellite-images-classification>  java -cp ./satellite-images-classification-1.0.0-bin.jar
rv.tech.service.ClassificationServiceImpl /home/dl/data/UCMerced_LandUse/Images /home/dl/data/models/satellite.bin 30
```

The above command will split the images in the images directory into training and test sets ( 80% for training/20% for
tests). After that it will iterate through all training examples, updating the unfrozen part of the graph. After each batch of 10 training examples the code will evaluate the model using the test data. A log file ("application.log") with the results will be saved to the "log" directory in the application root dir.


After 30 epochs the new trained model achieves an accuracy of 91% on the test set:
```
========================Evaluation Metrics========================
# of classes:    21
Accuracy:        0,9167
Precision:       0,9189
Recall:          0,9167
F1 Score:        0,9159
Precision, recall & F1: macro-averaged (equally weighted avg. of 21 classes)


=========================Confusion Matrix=========================
0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20
----------------------------------------------------------------
20  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 | 0 = agricultural
0 20  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 | 1 = airplane
0  0 18  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  2 | 2 = baseballdiamond
0  0  0 20  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 | 3 = beach
0  0  0  0 15  0  1  0  0  0  0  1  1  1  0  0  0  1  0  0  0 | 4 = buildings
0  0  0  0  0 20  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 | 5 = chaparral
0  0  0  0  2  0 14  0  0  0  0  0  3  0  0  1  0  0  0  0  0 | 6 = denseresidential
0  0  0  0  0  0  0 20  0  0  0  0  0  0  0  0  0  0  0  0  0 | 7 = forest
1  0  0  0  0  0  0  0 19  0  0  0  0  0  0  0  0  0  0  0  0 | 8 = freeway
0  0  0  0  0  0  0  1  0 18  0  0  0  0  0  0  1  0  0  0  0 | 9 = golfcourse
0  0  0  0  0  0  0  0  0  0 20  0  0  0  0  0  0  0  0  0  0 | 10 = harbor
0  0  0  0  0  0  0  0  0  0  0 18  1  0  1  0  0  0  0  0  0 | 11 = intersection
0  0  0  0  1  0  0  0  0  0  0  0 19  0  0  0  0  0  0  0  0 | 12 = mediumresidential
0  0  0  0  0  0  0  0  0  0  0  0  1 18  0  0  0  0  1  0  0 | 13 = mobilehomepark
0  0  0  0  0  0  0  0  0  0  0  0  0  0 20  0  0  0  0  0  0 | 14 = overpass
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 20  0  0  0  0  0 | 15 = parkinglot
0  0  0  1  0  0  0  2  0  0  0  0  0  0  0  0 17  0  0  0  0 | 16 = river
0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0 19  0  0  0 | 17 = runway
0  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0 19  0  0 | 18 = sparseresidential
0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1 18  1 | 19 = storagetanks
0  0  0  0  2  0  3  0  0  0  0  0  1  0  0  0  0  0  0  0 14 | 20 = tenniscourt
```



