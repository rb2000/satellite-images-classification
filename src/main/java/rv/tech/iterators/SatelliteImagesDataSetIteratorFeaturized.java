/*******************************************************************************
 * Copyright (c) 2020 Konduit K.K.
 * Copyright (c) 2015-2019 Skymind, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 ******************************************************************************/

package rv.tech.iterators;

import lombok.extern.slf4j.Slf4j;
import org.deeplearning4j.examples.advanced.features.transferlearning.editlastlayer.presave.FeaturizedPreSave;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.nd4j.linalg.dataset.AsyncDataSetIterator;
import org.nd4j.linalg.dataset.ExistingMiniBatchDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.io.IOException;

/**
 * !! original code was changed to process the satellite images
 * <p>
 * Iterator for featurized data.
 * original code author:  @author susaneraly on 3/10/17.
 */
@Slf4j
public class SatelliteImagesDataSetIteratorFeaturized {

    private static String featureExtractorLayer = FeaturizedPreSave.featurizeExtractionLayer;

    public static DataSetIterator trainIterator(String satelliteImagesDirectory, ComputationGraph vgg16Cloned) throws UnsupportedKerasConfigurationException, IOException, InvalidKerasConfigurationException {
        runFeaturize(satelliteImagesDirectory, vgg16Cloned);
        DataSetIterator existingTrainingData = new ExistingMiniBatchDataSetIterator(new File("trainFolder"), "satellite-images-" + featureExtractorLayer + "-train-%d.bin");
        DataSetIterator asyncTrainIter = new AsyncDataSetIterator(existingTrainingData);

        return asyncTrainIter;
    }

    public static DataSetIterator testIterator() {
        DataSetIterator existingTestData = new ExistingMiniBatchDataSetIterator(new File("testFolder"), "satellite-images-" + featureExtractorLayer + "-test-%d.bin");
        DataSetIterator asyncTestIter = new AsyncDataSetIterator(existingTestData);
        return asyncTestIter;
    }

    private static void runFeaturize(String satelliteImagesDirectory, ComputationGraph vgg16Cloned) throws InvalidKerasConfigurationException, IOException, UnsupportedKerasConfigurationException {
        File trainDir = new File("trainFolder", "satellite-images-" + featureExtractorLayer + "-train-0.bin");
        if (!trainDir.isFile()) {
            log.info("\n\tFEATURIZED DATA NOT FOUND. \n\t\tRUNNING \"FeaturizedPreSave\" first to do presave of featurized data");
            FeaturizedPreSave.trainNetwork(satelliteImagesDirectory, vgg16Cloned);
        }
    }
}
