package rv.tech.service;

import lombok.extern.slf4j.Slf4j;
import org.deeplearning4j.examples.advanced.features.transferlearning.editlastlayer.presave.FeaturizedPreSave;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.transferlearning.FineTuneConfiguration;
import org.deeplearning4j.nn.transferlearning.TransferLearning;
import org.deeplearning4j.nn.transferlearning.TransferLearningHelper;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.model.VGG16;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import rv.tech.iterators.SatelliteImagesDataSetIteratorFeaturized;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * FORK from DL4J exmaples github repo
 * <p>
 * This code is basically a copy of the "FitFromFeaturized.java"  that is in the
 * package "org.deeplearning4j.examples.advanced.features.transferlearning.editlastlayer.presave"
 * ( see the original at https://github.com/eclipse/deeplearning4j-examples/blob/master/dl4j-examples/src/main/java/org/deeplearning4j/examples/advanced/features/transferlearning/editlastlayer/presave/FitFromFeaturized.java")
 * So the credit go to the authors, I just adapted that code to use the satellite images.
 * <p>
 * The dataset used is the 'umerced land use' dataset:
 * Yi Yang and Shawn Newsam, "Bag-Of-Visual-Words and Spatial Extensions for Land-Use Classification," ACM SIGSPATIAL International Conference on Advances in Geographic Information Systems (ACM GIS), 2010.
 */
@Slf4j
public class ClassificationServiceImpl implements ClassificationService {

    private static final String featureExtractionLayer = FeaturizedPreSave.featurizeExtractionLayer;
    protected static final long seed = 12345;
    protected static final int numClasses = 21; // we have 21 image categories

    /**
     * needs to parameters: the parent dir where the subdirs with the satellite images are,
     * and the  model file save  path ( absolute path ).
     *
     * @param args
     * @throws IOException
     * @throws UnsupportedKerasConfigurationException
     * @throws InvalidKerasConfigurationException
     */
    public static void main(String[] args) throws IOException, UnsupportedKerasConfigurationException, InvalidKerasConfigurationException {
        ClassificationService cs = new ClassificationServiceImpl();
        cs.startTraining(args[0], args[1], Integer.parseInt(args[2]));
    }


    @Override
    public TransferLearningHelper startTraining(String pathToImagesParentDir,
                                                String pathToSavedModel, int numberOfEpochs) throws IOException, UnsupportedKerasConfigurationException, InvalidKerasConfigurationException {

        log.info("\n\nLoading org.deeplearning4j.transferlearning.vgg16...\n\n");

        ZooModel zooModel = VGG16.builder().build();
        ComputationGraph temp = (ComputationGraph) zooModel.initPretrained();
        ComputationGraph vgg16Clone = temp.clone();
        log.info(vgg16Clone.summary());

        FineTuneConfiguration fineTuneConf = new FineTuneConfiguration.Builder()
                .updater(new Nesterovs(3e-5, 0.9))
                .seed(seed)
                .build();

        ComputationGraph vgg16Transfer = new TransferLearning.GraphBuilder(vgg16Clone)
                .fineTuneConfiguration(fineTuneConf)
                .setFeatureExtractor(featureExtractionLayer) //the specified layer and below are "frozen"
                .removeVertexKeepConnections("predictions") //replace the functionality of the final vertex
                .addLayer("predictions",
                        new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                                .nIn(4096).nOut(numClasses)
                                .weightInit(new NormalDistribution(0, 0.2 * (2.0 / (4096 + numClasses)))) //This weight init dist gave better results than Xavier
                                .activation(Activation.SOFTMAX).build(),
                        "fc2")
                .build();
        log.info(vgg16Transfer.summary());

        DataSetIterator trainIter = SatelliteImagesDataSetIteratorFeaturized.trainIterator(pathToImagesParentDir, vgg16Transfer);
        DataSetIterator testIter = SatelliteImagesDataSetIteratorFeaturized.testIterator();


        TransferLearningHelper transferLearningHelper = new TransferLearningHelper(vgg16Transfer);
        log.info(transferLearningHelper.unfrozenGraph().summary());
        List<String> labels = getLabels(pathToImagesParentDir);
        for (int epoch = 0; epoch < numberOfEpochs; epoch++) {
            if (epoch == 0) {
                Evaluation eval = transferLearningHelper.unfrozenGraph().evaluate(testIter);
                eval.setLabelsList(labels);
                log.info("Eval stats BEFORE fit....." + labels);
                log.info(eval.stats(false, true) + "\n");
                testIter.reset();
            }
            int iter = 0;
            while (trainIter.hasNext()) {
                transferLearningHelper.fitFeaturized(trainIter.next());
                if (iter % 10 == 0) {
                    log.info("Evaluate model at iter " + iter + " ....");
                    Evaluation eval = transferLearningHelper.unfrozenGraph().evaluate(testIter);
                    eval.setLabelsList(labels);
                    log.info(eval.stats(false, true));
                    if (testIter.hasNext()) {
                        org.nd4j.linalg.dataset.DataSet ds = testIter.next();
                        INDArray features = ds.getFeatures();
                        INDArray results = transferLearningHelper.unfrozenGraph().outputSingle(features);
                        log.info(iter + " ############## --- " + results);
                    }
                    testIter.reset();
                }
                iter++;
            }

            trainIter.reset();
            log.info("Epoch #" + epoch + " complete");
        }
        ModelSerializer.writeModel(vgg16Transfer, new File(pathToSavedModel), false);
        log.info("Model build complete");
        return transferLearningHelper;
    }

    /**
     * added this method to show the confusion matrix with labels, somehow the original
     * code just displayed numbers instead of the directory names ..
     *
     * @param pathToParentDir
     * @return
     */
    private static List<String> getLabels(String pathToParentDir) {
        File file = new File(pathToParentDir);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        return Arrays.asList(directories != null ? directories : new String[0]);
    }
}
