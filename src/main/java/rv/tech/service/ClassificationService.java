package rv.tech.service;

import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.transferlearning.TransferLearningHelper;

import java.io.IOException;

public interface ClassificationService {
    TransferLearningHelper startTraining(String pathToImagesParentDir, String pathToSaveModelsDir, int numberOfEpochs) throws IOException, UnsupportedKerasConfigurationException, InvalidKerasConfigurationException;
}
